<?php
/*Template Name: home page */
get_header();
?>


<!--==========================
  Intro Section
============================-->
<?php if (have_rows('intro_section')): ?>
    <section id="intro">
        <?php while (have_rows('intro_section')): the_row();
            $title = get_sub_field('title');
            $text = get_sub_field('text');
            ?>
            <div class="intro-text">
                <h2><?= $title; ?></h2>
                <p><?= $text; ?></p>
                <a href="#about" class="btn-get-started scrollto">Get Started</a>
            </div>
            <?php
            $count_screens = 1;
            if (have_rows('product_screens')): ?>
                <div class="product-screens">
                    <?php while (have_rows('product_screens')): the_row();
                        $screen = get_sub_field('screen');
                        ?>
                        <div class="<?= 'product-screen-' . $count_screens; ?>" data-aos="fade-up" data-aos-delay="400">
                            <img src="<?= $screen['url'] ?>" alt="<?= $screen['alt'] ?>">
                        </div>
                        <?php $count_screens++; endwhile; ?>
                </div>
            <?php endif; ?>

        <?php endwhile; ?>
    </section><!-- #intro -->
<?php endif; ?>


<main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <?php if (have_rows('about_us_section')): ?>
        <section id="about" class="section-bg">
            <div class="container-fluid">
                <?php while (have_rows('about_us_section')):
                the_row();
                $title = get_sub_field('title');
                $text = get_sub_field('text');
                $img = get_sub_field('about_img');
                ?>
                <div class="section-header">
                    <h3 class="section-title"><?= $title; ?></h3>
                    <span class="section-divider"></span>
                    <div class="section-description"><?= $text; ?></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 about-img" data-aos="fade-right">
                        <img src="<?= $img['url'] ?>" alt="<?= $img['alt'] ?>">
                    </div>
                    <?php if (have_rows('about_content')): ?>
                        <div class="col-lg-6 content" data-aos="fade-left">
                            <?php while (have_rows('about_content')): the_row();
                                $title = get_sub_field('title');
                                $text_1 = get_sub_field('text_1');
                                $text_2 = get_sub_field('text_2');
                                $text_3 = get_sub_field('text_3');
                                ?>
                                <h2><?= $title; ?></h2>
                                <h3><?= $text_1; ?></h3>
                                <p>  <?= $text_2; ?></p>

                                <?php if (have_rows('lists')): ?>
                                    <ul>
                                        <?php while (have_rows('lists')): the_row();
                                            $list = get_sub_field('list');
                                            ?>
                                            <li><i class="ion-android-checkmark-circle"></i> <?= $list; ?> </li>
                                        <?php endwhile; ?>
                                    </ul>
                                <?php endif; ?>

                                <p><?= $text_3; ?></p>

                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>

                    <?php endwhile; ?>
                </div>
            </div><!-- #about -->
        </section>
    <?php endif; ?>

    <!--==========================
      Product Featuress Section
    ============================-->
    <?php if (have_rows('product_featuress_section')): ?>
        <section id="features">
            <div class="container">
                <div class="row">
                    <?php while (have_rows('product_featuress_section')): the_row();
                        $title = get_sub_field('title');
                        $features_img = get_sub_field('features_img');
                        ?>
                        <div class="col-lg-8 offset-lg-4">
                            <div class="section-header" data-aos="fade" data-aos-duration="1000">
                                <h3 class="section-title"><?= $title; ?></h3>
                                <span class="section-divider"></span>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-5 features-img">
                            <img src="<?= $features_img['url']; ?>" alt="<?= $features_img['alt']; ?>"
                                 data-aos="fade-right" data-aos-easing="ease-out-back">
                        </div>
                        <?php

                        if (have_rows('products_features')): ?>
                            <div class="col-lg-8 col-md-7 ">
                            <div class="row">
                            <?php $aos_delay = 0;
                            while (have_rows('products_features')): the_row();
                                $class_ico = get_sub_field('class_ico');
                                $title = get_sub_field('title');
                                $description = get_sub_field('description');
                                ?>
                                <div class="col-lg-6 col-md-6 box" data-aos="fade-left"
                                     data-aos-delay="<?= $aos_delay; ?>">
                                    <div class="icon"><i class="<?= $class_ico; ?>"></i></div>
                                    <h4 class="title"><a href=""><?= $title; ?></a></h4>
                                    <div class="description"><?= $description; ?></div>
                                </div>
                                <?php $aos_delay = $aos_delay + 100; endwhile; ?>

                        <?php endif; ?>
                        </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </section><!-- #features -->
    <?php endif; ?>


    <!--==========================
      Product Advanced Featuress Section
    ============================-->

    <?php if (have_rows('product_advanced_featuress_section')):
        $n = 1;
        ?>
        <section id="advanced-features">
            <?php while (have_rows('product_advanced_featuress_section')): the_row();
                $features_img = get_sub_field('advanced_feature_img');
                $title = get_sub_field('title');
                $title2 = get_sub_field('title_2');
                $text = get_sub_field('text');
                $text2 = get_sub_field('text_2');
                ?>


                <div class="features-row <?php if ($n % 2 !== 0) {
                    echo 'section-bg';
                } ?>">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <img class="<?php if ($n % 2 != 0) {
                                    echo 'advanced-feature-img-right';
                                } else {
                                    echo 'advanced-feature-img-left';
                                } ?>"
                                     src="<?= $features_img['url']; ?>"
                                     alt="<?= $features_img['alt']; ?>" data-aos="fade-left">
                                <div data-aos="fade-right">
                                    <h2><?= $title; ?></h2>
                                    <h3><?= $title2; ?></h3>
                                    <div><?= $text; ?></div>
                                    <div><?= $text2; ?></div>


                                    <?php
                                    if (have_rows('list')): ?>
                                        <?php $aos_delay = 0;
                                        while (have_rows('list')): the_row();
                                            $class_ico = get_sub_field('ico');
                                            $text = get_sub_field('text');
                                            ?>

                                            <i class="<?= $class_ico; ?>" data-aos="fade-left"
                                               data-aos-delay="<?= $aos_delay; ?>"></i>
                                            <div data-aos="fade-left"
                                                 data-aos-delay="<?= $aos_delay; ?>"><?= $text; ?></div>

                                            <?php $aos_delay = $aos_delay + 200; endwhile; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <?php $n++; endwhile; ?>
        </section><!-- #advanced-features -->
    <?php endif; ?>


    <!--==========================
      Call To Action Section
    ============================-->
    <?php if (have_rows('call_to_action_section')): ?>
        <section id="call-to-action">
            <div class="container">
                <div class="row">
                    <?php while (have_rows('call_to_action_section')): the_row();
                        $title = get_sub_field('title');
                        $description = get_sub_field('description');
                        ?>
                        <div class="col-lg-9 text-center text-lg-left">
                            <h3 class="cta-title"><?= $title; ?></h3>
                            <div class="cta-text"><?= $description; ?></div>
                        </div>
                        <div class="col-lg-3 cta-btn-container text-center">
                            <a class="cta-btn align-middle" href="#">Call To Action</a>
                        </div>


                    <?php endwhile; ?>
                </div>
            </div>
        </section><!-- #call-to-action -->
    <?php endif; ?>


    <!--==========================
      More Features Section
    ============================-->
    <?php if (have_rows('more_features_section')): ?>
        <section id="more-features" class="section-bg">
            <div class="container">
                <?php while (have_rows('more_features_section')): the_row();
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                    ?>
                    <div class="section-header">
                        <h3 class="section-title"><?= $title; ?></h3>
                        <span class="section-divider"></span>
                        <div class="section-description"><?= $description; ?></div>
                    </div>

                    <?php
                    if (have_rows('list_more_features')): ?>
                        <div class="row">
                            <?php $n = 1;
                            while (have_rows('list_more_features')): the_row();
                                $class_ico = get_sub_field('class_ico');
                                $title = get_sub_field('title');
                                $description = get_sub_field('description');
                                ?>


                                <div class="col-lg-6">
                                    <div class="box" data-aos="<?php if ($n % 2 != 0) {
                                        echo 'fade-right';
                                    } else {
                                        echo 'fade-left';
                                    } ?>">
                                        <div class="icon"><i class="<?= $class_ico; ?>"></i></div>
                                        <h4 class="title"><a href=""><?= $title; ?></a></h4>
                                        <div class="description"><?= $description; ?></div>
                                    </div>
                                </div>

                                <?php $n++; endwhile; ?>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>

            </div>
        </section><!-- #more-features -->
    <?php endif; ?>


    <!--==========================
      Clients
    ============================-->
    <?php if (have_rows('clients')): ?>
        <section id="clients">
            <div class="container">

                <div class="row">

                    <?php while (have_rows('clients')): the_row();
                        $client = get_sub_field('client');
                        ?>
                        <div class="col-md-2">
                            <img src="<?= $client['url']; ?>"
                                 alt="<?= $client['alt']; ?>"
                                 data-aos="fade-up">
                        </div>
                    <?php endwhile; ?>

                </div>
            </div>
        </section><!-- #more-features -->
    <?php endif; ?>


    <!--==========================
      Pricing Section
    ============================-->
    <?php if (have_rows('pricing_section')): ?>
        <section id="pricing" class="section-bg">
            <div class="container">
                <?php while (have_rows('pricing_section')): the_row();
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                    ?>
                    <div class="section-header">
                        <h3 class="section-title"><?= $title; ?></h3>
                        <span class="section-divider"></span>
                        <div class="section-description"><?= $description; ?></div>
                    </div>

                    <?php
                    if (have_rows('price_cards')): ?>
                        <div class="row">
                            <?php $n = 1;
                            while (have_rows('price_cards')): the_row();
                                $title = get_sub_field('title_card');
                                $currency = get_sub_field('currency');
                                $price = get_sub_field('price');
                                $how_much_time = get_sub_field('how_much_time');
                                ?>


                                <div class="col-lg-4 col-md-6">
                                    <div class="box <?php if ($n % 2 == 0) {
                                        echo 'featured';
                                    } ?>" data-aos="<?php if ($n == 1) {
                                        echo 'fade-right';
                                    } elseif ($n == 2) {
                                        echo 'fade-up';
                                    } elseif ($n == 3) {
                                        echo 'fade-left';
                                    } ?>">
                                        <h3><?= $title; ?></h3>
                                        <h4><sup><?= $currency; ?></sup><?= $price; ?>
                                            <span><?= $how_much_time; ?></span></h4>

                                        <?php
                                        if (have_rows('list_of_conditions')): ?>
                                            <ul>
                                                <?php while (have_rows('list_of_conditions')): the_row();
                                                    $text = get_sub_field('text');
                                                    ?>
                                                    <li><i class="ion-android-checkmark-circle"></i> <?= $text; ?>
                                                    </li>
                                                <?php endwhile; ?>
                                            </ul>
                                        <?php endif; ?>
                                        <a href="#" class="get-started-btn">Get Started</a>
                                    </div>
                                </div>

                                <?php $n++; endwhile; ?>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
        </section><!-- #pricing -->
    <?php endif; ?>


    <!--==========================
      Frequently Asked Questions Section
    ============================-->
    <?php if (have_rows('frequently_asked_questions_section')): ?>
        <section id="faq">
            <div class="container">
                <?php while (have_rows('frequently_asked_questions_section')): the_row();
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                    ?>
                    <div class="section-header">
                        <h3 class="section-title"><?= $title; ?></h3>
                        <span class="section-divider"></span>
                        <div class="section-description"><?= $description; ?></div>
                    </div>

                    <?php
                    if (have_rows('list_of_questions')): ?>
                        <ul id="faq-list" data-aos="fade-up">
                            <?php
                            $count_question = 1;
                            while (have_rows('list_of_questions')): the_row();
                                $question = get_sub_field('question');
                                $answer = get_sub_field('answer');
                                ?>
                                <li>
                                    <a data-toggle="collapse" class="collapsed"
                                       href="<?= '#faq' . $count_question; ?>"><?= $question; ?>
                                        <i class="ion-android-remove"></i></a>
                                    <div id="<?= 'faq' . $count_question; ?>" class="collapse" data-parent="#faq-list">
                                        <p>
                                            <?= $answer; ?>
                                        </p>
                                    </div>
                                </li>

                                <?php $count_question++; endwhile; ?>
                        </ul>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
        </section><!-- #faq -->
    <?php endif; ?>
    <!--==========================
      Our Team Section
    ============================-->
    <?php if (have_rows('our_team_section')): ?>
        <section id="team" class="section-bg">
            <div class="container">
                <?php while (have_rows('our_team_section')): the_row();
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                    ?>
                    <div class="section-header">
                        <h3 class="section-title"><?= $title; ?></h3>
                        <span class="section-divider"></span>
                        <div class="section-description"><?= $description; ?></div>
                    </div>

                    <?php
                    if (have_rows('member')): ?>
                        <div class="row" data-aos="fade-up">
                            <?php
                            while (have_rows('member')): the_row();
                                $foto = get_sub_field('foto');
                                $name = get_sub_field('name');
                                $position = get_sub_field('position');
                                ?>
                                <div class="col-lg-3 col-md-6">
                                    <div class="member">
                                        <div class="pic"><img
                                                    src="<?= $foto['url']; ?>"
                                                    alt="<?= $foto['alt']; ?>"></div>
                                        <h4><?= $name; ?></h4>
                                        <span><?= $position; ?></span>

                                        <?php
                                        if (have_rows('social')): ?>
                                            <div class="social">
                                                <?php while (have_rows('social')): the_row();
                                                    $name_social = get_sub_field('name_social');
                                                    $link_social = get_sub_field('link_social');
                                                    ?>

                                                    <a href="<?= $link_social; ?>"><i
                                                                class="fa <?= 'fa-' . $name_social; ?>"></i></a>

                                                <?php endwhile; ?>
                                            </div>
                                        <?php endif; ?>

                                    </div>
                                </div>

                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
        </section><!-- #team -->
    <?php endif; ?>

    <!--==========================
      Gallery Section
    ============================-->

    <?php if (have_rows('gallery_section')): ?>
        <section id="gallery">
            <div class="container-fluid">
                <?php while (have_rows('gallery_section')): the_row();
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                    $images = get_sub_field('gallery');
                    ?>
                    <div class="section-header">
                        <h3 class="section-title"><?= $title; ?></h3>
                        <span class="section-divider"></span>
                        <div class="section-description"><?= $description; ?></div>
                    </div>

                    <?php
                    if ($images):
                        ?>
                        <div class="row no-gutters">
                            <?php foreach ($images as $image):
                                ?>
                                <div class="col-lg-4 col-md-6">
                                    <div class="gallery-item" data-aos="fade-up">
                                        <a href="<?php echo $image['sizes']['medium_large']; ?>" class="gallery-popup">
                                            <img src="<?php echo $image['sizes']['medium_large']; ?>"
                                                 alt="<?php echo $image['alt']; ?>">
                                        </a>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>


                <?php endwhile; ?>
            </div>
        </section><!-- #gallery -->
    <?php endif; ?>

    <!--==========================
      Contact Section
    ============================-->
    <?php if (have_rows('contact_section')): ?>
        <section id="contact">
            <div class="container">
                <div class="row" data-aos="fade-up">
                    <?php while (have_rows('contact_section')): the_row();
                        $title = get_sub_field('title');
                        $description = get_sub_field('description');
                        ?>


                        <div class="col-lg-4 col-md-4">
                            <div class="contact-about">
                                <h3><?= $title; ?></h3>
                                <div><?= $description; ?></div>


                                <?php
                                if (have_rows('social')): ?>
                                    <div class="social-links">
                                        <?php  while (have_rows('social')): the_row();
                                            $name_social = get_sub_field('name_social');
                                            $link_social = get_sub_field('link_social');
                                            ?>
                                            <a href="<?= $link_social; ?>" class="<?= $name_social; ?>"><i class="fa <?= 'fa-' . $name_social; ?>"></i></a>

                                        <?php endwhile; ?>
                                    </div>
                                <?php endif; ?>

                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                                <?php
                                if (have_rows('contacts')): ?>
                                    <div class="info">
                                        <?php  while (have_rows('contacts')): the_row();
                                            $class_ico = get_sub_field('class_ico');
                                            $info = get_sub_field('info');
                                            ?>
                                            <div>
                                                <i class="<?= $class_ico; ?>"></i>
                                                <div><?= $info; ?></div>
                                            </div>

                                        <?php endwhile; ?>
                                    </div>
                                <?php endif; ?>


                        </div>

                        <div class="col-lg-5 col-md-8">
                            <div class="form">
                                <?= do_shortcode('[contact-form-7 id="14" title="Contact form"]'); ?>
                            </div>
                        </div>


                    <?php endwhile; ?>
                </div>
            </div>
        </section><!-- #contact -->
    <?php endif; ?>

</main>

<?php get_footer(); ?>

