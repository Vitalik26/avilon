<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'avilon_master');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**Remove <p> and <br /> from Contact Form 7**/
define('WPCF7_AUTOP', false );
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',qJPI5>q R6RepI w*Yha|b]ssaY9GoI7>oDp}sS_N)q=Q_q^/x(@Oo?,daTkOi(');
define('SECURE_AUTH_KEY',  'j[xs%MZ%mc*FGAyI}$ fP6Rz Jr~.44q8u)m)u]G+I^bn6PKO.N)O-Bgf`,)W45/');
define('LOGGED_IN_KEY',    'pM(!JQ$YBbt}4Nk|4 {p;.D<r06b|]#A]Z&*`^ZxSmB;2Mdw]<h67Z&*i^PS+_ D');
define('NONCE_KEY',        'g}&b`-<nYRqw!N3];vre;[H|<{ba,yIS)hvKB%._|dpT@TtfLpUl!5^sB}E-i]2T');
define('AUTH_SALT',        '/ ;iUffE&!kl_5Z,Aa#4{f=bD(1K~9in{L9v91g<<?OBz>v;:hQ+{L=+onje9;hj');
define('SECURE_AUTH_SALT', 'xi=C5H*J|do!rhy|Z/ |K;-sX<cU92l|~chF{m~Wu8sNB7WC(rAI@JUMDAX.(#oy');
define('LOGGED_IN_SALT',   '&0-y8JdLT8B9OsK(zosFj=2Rvn4N4jg2l9]tF,UmPla8rtc+i9J@Y(w;Ln8jY6uL');
define('NONCE_SALT',       'X RX2E.ZA{SXk0ISBO%qhXWRm14Lux,Tv$TKnA31YgpX;@oj1_ ZKafI]|>w/MO4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
